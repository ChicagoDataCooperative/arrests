# Arrests
ETL for Chicago Police Arrests

```bash
createdb arrests
blackbox_postdeploy
pip install -r requirements
make load_data
```

If you need blackbox credentials, see https://github.com/datamade/ops/wiki/Setup-blackbox


